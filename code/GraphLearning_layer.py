import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.parameter import Parameter
import numpy as np
from sklearn.preprocessing import normalize
import math
from torch.nn.utils import weight_norm

        
class LowRankGraph(nn.Module):
    def __init__(self, args, data, hidden):
        super(LowRankGraph, self).__init__()
        self.m = data.m
        self.use_cuda = args.cuda
        self.Q = hidden
        
        self.low_rank = args.low_rank     
        self.Identity = nn.Parameter(torch.ones(self.m))
        self.LeftGraph = nn.Linear(self.m, self.low_rank, bias = False)    
        self.RightGraph = nn.Linear(self.low_rank, self.m, bias = False)
        #orch.nn.init.orthogonal_(self.LeftGraph.weight)
        #orch.nn.init.orthogonal_(self.RightGraph.weight)
        
        self.LeftGraph.weight.data = nn.Parameter((1/self.m)*torch.ones(self.low_rank, self.m))
        self.RightGraph.weight.data = nn.Parameter((1/self.m)*torch.ones(self.m, self.low_rank))

    def forward(self, x_sp, batch_size):            
        self.LeftGraph.weight.data = F.normalize(self.LeftGraph.weight.data, p=2, dim=1, eps=1e-10)
        self.RightGraph.weight.data = F.normalize(self.RightGraph.weight.data, p=2, dim=0, eps=1e-10)
        x_sp = self.RightGraph(self.LeftGraph(x_sp)) + self.Identity*x_sp        
        return x_sp
    