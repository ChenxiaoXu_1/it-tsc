import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.parameter import Parameter
from torch.nn.utils import weight_norm

class DilatedCausalConv(nn.Module):
    def __init__(self, args, dilation_size, in_channel, out_channel):
        super(DilatedCausalConv, self).__init__()

        self.kernel_size = args.kernel_size1
        self.padding_size = (self.kernel_size-1) * dilation_size
        self.in_channel = in_channel
        self.out_channel = out_channel
        self.double_causal_perLayer = args.double_causal_perLayer
        self.ResNet_causal_conv = args.ResNet_causal_conv
        self.dropout = args.dropout1
        
        self.CausalLayer_list = []
    
        CausalLayer = weight_norm(nn.Conv1d(self.in_channel, self.out_channel, self.kernel_size, stride=1, padding=self.padding_size, dilation = dilation_size))
        CausalLayer.weight.data.normal_(0, 1e-10)        
        self.CausalLayer_list.append(CausalLayer)
        
        if self.double_causal_perLayer:
            CausalLayer = weight_norm(nn.Conv1d(self.out_channel, self.out_channel, self.kernel_size, stride=1, padding=self.padding_size, dilation = dilation_size))
            CausalLayer.weight.data.normal_(0, 1e-10)        
            self.CausalLayer_list.append(CausalLayer)
        
        if self.ResNet_causal_conv:
            ResidualLayer = nn.Conv1d(self.in_channel, self.out_channel, 1)
            ResidualLayer.weight.data.normal_(0, 1e-10)
            self.CausalLayer_list.append(ResidualLayer)
        
        self.CausalLayer_list = nn.ModuleList(self.CausalLayer_list)
        self.dropout = nn.Dropout(self.dropout)
        
    def forward(self, x0):
        batch_size, self.m = x0.shape[0], x0.shape[1] #x0: (batch, m, channel, hidden_timestamp)

        x1 = x0.reshape((batch_size*self.m, self.in_channel, -1)) #(batch_size*m, channel, hidden_timestamp)
        x_org = x1.clone()
       
        x1 = self.CausalLayer_list[0](x1) 
        x1 = x1[:,:,:-self.padding_size]
        x1 = F.elu(x1)  #celu, elu, 
        x1 = self.dropout(x1)

        
        if self.double_causal_perLayer:        
            x1 = self.CausalLayer_list[1](x1) 
            x1 = x1[:,:,:-self.padding_size]
            x1 = F.elu(x1)       
            x1 = self.dropout(x1)

                    
        if self.ResNet_causal_conv:
            x_res = self.CausalLayer_list[-1](x_org)
            # x1 = (x_res + x1)
            x1 = F.elu(x_res + x1)
        
        x1 = x1.reshape((batch_size, self.m, self.out_channel, -1)) #(batch_size, m, channel, hidden_timestamp)
        return x1