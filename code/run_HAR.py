# -*- coding: utf-8 -*-
import argparse
import math
import time
import torch
import torch.nn as nn
import torch.nn.functional as functional
import numpy as np
import DCM_DeepCausal_CausalConv, DilatedCausalConv_layer, FinalRowWise_layer, GraphLearning_layer, Optim, utils_realdata
import scipy
import sklearn
from sklearn import metrics
import matplotlib.pyplot as plt
from sklearn import preprocessing
from sklearn.metrics import normalized_mutual_info_score, accuracy_score, adjusted_rand_score
from torch.autograd import Variable
import os


import warnings
warnings.filterwarnings("ignore")

parser = argparse.ArgumentParser(description= 'MultPath Cluster')
parser.add_argument('--data_path', type = str,  default = '../data/')
parser.add_argument('--data_name', type = str,  default = 'HAR')
parser.add_argument('--data_source', type = str,  default = 'Karim')

parser.add_argument('--model', type = str, default = 'DCM_DeepCausal_CausalConv')

parser.add_argument('--kernel_size1', type = int, default = 4)
parser.add_argument('--NumCausalLayer1', type = int, default = 5)
parser.add_argument('--channel_multiplier', type = int, default = 3)

parser.add_argument('--double_causal_perLayer', type = bool, default = True)
parser.add_argument('--ResNet_causal_conv', type = bool, default = True)

parser.add_argument('--low_rank', type = int, default = 3)
parser.add_argument('--Graph_reg', type = float, default = 0.0)
parser.add_argument('--Graph_activate', type = str, default = 'tanh')

parser.add_argument('--tanh_alpha', type = float, default = 5.0)
parser.add_argument('--reduce_ele', type = int, default = 50)
parser.add_argument('--pre_win', type = int, default = 1)
parser.add_argument('--pred_start_point', type = int, default = 1)
parser.add_argument('--data_normalization', type = str, default = 'global_standard')
parser.add_argument('--data_rolling_mean', type = int, default = 0)
parser.add_argument('--epochs', type = int, default = 4)
parser.add_argument('--batch_size', type = int, default = 32)
parser.add_argument('--dropout1', type = float, default = 0.05) #dropout for module 1
parser.add_argument('--dropout', type = float, default = 0.1)
parser.add_argument('--seed', type = int, default = 12345)
parser.add_argument('--lr', type = float, default = 0.0005)
parser.add_argument('--lr_decay', type = float,  default = 0.99)
parser.add_argument('--start_decay_at', type = int,  default = 1)
parser.add_argument('--weight_decay', type = float,  default = 0)
parser.add_argument('--optim', type = str,  default = 'adam')
parser.add_argument('--random_shuffle', type = bool, default = True)
parser.add_argument('--clip', type = float,  default = 1.)

parser.add_argument('--gpu', type = int, default = 0)
parser.add_argument('--cuda', type = str, default = True)

args = parser.parse_args()
args.reduce_dim = [args.reduce_ele]
args.channel_list = [3,9,27,81,243]
if args.cuda:
    torch.cuda.set_device(args.gpu)

if torch.cuda.is_available() and not args.cuda:
    print("WARNING: You have a CUDA device, so you should probably run with --cuda")
        
criterion = nn.MSELoss(size_average=True, reduce=False) #Take the sum, not the average
if args.cuda:
    criterion = criterion.cuda()

def find_permutation(n_clusters, real_labels, labels):
    permutation=[]
    for i in range(n_clusters):
        idx = labels == i
        new_label=scipy.stats.mode(real_labels[idx])[0][0]  # Choose the most common label among data points in the cluster
        permutation.append(new_label)
    return permutation

Data = utils_realdata.Data_utility(args)
args.NumCluster = Data.NumCluster
args.MaxLen = Data.MaxLen

seed = 0
np.random.seed(seed)
torch.manual_seed(seed)
if args.cuda is True:
    torch.cuda.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    torch.backends.cudnn.enabled = False 
    torch.backends.cudnn.benchmark = False
    torch.backends.cudnn.deterministic = True

model = eval(args.model).Model(args, Data)
if args.cuda:
    model.cuda()
optim = Optim.Optim(model.parameters(), args.optim, args.lr, args.clip, lr_decay = args.lr_decay, start_decay_at = args.start_decay_at, weight_decay = args.weight_decay)

nParams = sum([p.nelement() for p in model.parameters()])
print('Number of parameters: %d' % nParams)


def test(loader, data, model, criterion, optim, args):
    total_loss = 0    
    total_loss_reg = 0
    total_loss_cos = 0
    total_loss_left = 0
    total_loss_right = 0

    n_samples = 0        
    total_time = 0
    
    count = 0
    
    pred_labels = []
    new_soft_vectors = []
    true_label_all = []    

    residuals_alldim_all = [ [] for _ in range(args.NumCluster) ]

    for inputs in loader.get_batches(data, args.batch_size, False):                
        begin_time = time.time() 
        
        [sequence, true_label, seqLen] = inputs #sequence: (batch_size, variables, timestamps)      
        true_label_all.append(true_label)
        
        batch_size = len(sequence)
        MaxLen = sequence.shape[-1]
        
        model.zero_grad()

        X = []
        Y_long = []
        Y_long_mask = []
        for seq_i in range(batch_size):
            Len_i = seqLen[seq_i]
            X_i = sequence[seq_i,:,:Len_i-args.pre_win]
            X_i = functional.pad(X_i, (0, MaxLen - Len_i), 'constant', 0)
            
            Y_i_long = []
            Y_i_long_mask = []
            for pre_win_j in range(1, args.pre_win+1):
                Y_i_long_j = sequence[seq_i,:,pre_win_j : Len_i-args.pre_win+pre_win_j]
                Y_i_long.append(Y_i_long_j)
                Y_i_long_mask.append(1.0*torch.ones(Y_i_long_j.shape))
                
            Y_i_long = torch.stack(Y_i_long, dim = 1) #(m, pre_win, MaxLen-pre_win)
            Y_i_long_mask = torch.stack(Y_i_long_mask, dim = 1)
            Y_i_long = functional.pad(Y_i_long, (0, MaxLen - Len_i), 'constant', 0)
            Y_i_long_mask = functional.pad(Y_i_long_mask, (0, MaxLen - Len_i), 'constant', 0)
            Y_i_long_mask[:,:,:args.pred_start_point-1] = 0.0
            X.append(X_i)
            Y_long.append(Y_i_long)
            Y_long_mask.append(Y_i_long_mask)
            
        X = torch.stack(X, dim = 0)  #(batch_size, variables, Max-pre_win)
        Y_long = torch.stack(Y_long, dim = 0) #(batch_size, variables, pre_win, MaxLen-pre_win)
        Y_long_mask = torch.stack(Y_long_mask, dim = 0) #(batch_size, variables, pre_win, MaxLen-pre_win)
        Y_long = Y_long.permute(0,3,2,1) #(batch_size, MaxLen - pre_win , pre_win, m)
        Y_long_mask = Y_long_mask.permute(0,3,2,1) #(batch_size, MaxLen - pre_win, pre_win, m)
                
        Y_hat_list = model(X) #(batch, Max-pre_win, pre_win, m)
        Y_long_mask = Variable(Y_long_mask)
        Y_long = Variable(Y_long)
        
        if args.cuda:
            Y_long_mask = Y_long_mask.cuda()
            Y_long = Y_long.cuda()
        
        residuals = []
        for cluster_i in range(args.NumCluster):
            residuals_tmp = criterion(Y_hat_list[cluster_i]*Y_long_mask, Y_long*Y_long_mask)
            residuals.append(torch.sum(residuals_tmp, dim=(1,2,3)))
            residuals_alldim_all[cluster_i].append(residuals_tmp.cpu().detach().numpy())
        
        residuals = torch.stack(residuals, dim = 1)
        new_soft_vectors.append(residuals)
        loss_reg = torch.sum(torch.min(residuals, dim=1)[0]) / (np.sum(Y_long_mask.cpu().detach().numpy())) 
        pred_labels.append(torch.min(residuals, dim=1)[1])
        
        GraphReg = 0.0

        batch_loss = loss_reg     
        total_loss += batch_loss.data.item()
        total_loss_reg += loss_reg.data.item()    
        #total_loss_left += GraphReg.item()
        total_loss_left += GraphReg

        total_time = total_time + time.time() - begin_time  
        
        optim.step()
        n_samples += batch_size
        count += 1

    total_loss /= (n_samples)
    total_loss_reg /= (n_samples)
    total_loss_cos /= count
    total_loss_left /= count
    total_loss_right /= count

    true_labels = np.concatenate(true_label_all, axis = 0)
    pred_labels = (torch.cat(pred_labels, dim = 0)).cpu().detach().numpy()

    for clust_i in range(args.NumCluster):
        residuals_alldim_all[clust_i] = np.concatenate(residuals_alldim_all[clust_i], axis = 0)

    new_soft_vectors = (torch.cat(new_soft_vectors, dim = 0)).cpu().detach().numpy()

    return total_time, total_loss, total_loss_reg, total_loss_cos, total_loss_left, total_loss_right, n_samples, pred_labels, true_labels, new_soft_vectors, residuals_alldim_all


def train(loader, data, model, criterion, optim, args):
    model.train()

    total_loss = 0    
    total_loss_reg = 0
    total_loss_cos = 0
    total_loss_left = 0
    total_loss_right = 0

    n_samples = 0        
    total_time = 0
    
    count = 0
    
    pred_labels = []
    new_soft_vectors = []
    true_label_all = []    

    residuals_alldim_all = [ [] for _ in range(args.NumCluster) ]

    for inputs in loader.get_batches(data, args.batch_size, True):                
        begin_time = time.time() 
        
        [sequence, true_label, seqLen] = inputs #sequence: (batch_size, variables, timestamps)      
        true_label_all.append(true_label)
        
        batch_size = len(sequence)
        MaxLen = sequence.shape[-1]
        
        model.zero_grad()

        X = []
        Y_long = []
        Y_long_mask = []
        for seq_i in range(batch_size):
            Len_i = seqLen[seq_i]
            X_i = sequence[seq_i,:,:Len_i-args.pre_win]
            X_i = functional.pad(X_i, (0, MaxLen - Len_i), 'constant', 0)
            
            Y_i_long = []
            Y_i_long_mask = []
            for pre_win_j in range(1, args.pre_win+1):
                Y_i_long_j = sequence[seq_i,:,pre_win_j : Len_i-args.pre_win+pre_win_j]
                Y_i_long.append(Y_i_long_j)
                Y_i_long_mask.append(1.0*torch.ones(Y_i_long_j.shape))
                
            Y_i_long = torch.stack(Y_i_long, dim = 1) #(m, pre_win, MaxLen-pre_win)
            Y_i_long_mask = torch.stack(Y_i_long_mask, dim = 1)
            Y_i_long = functional.pad(Y_i_long, (0, MaxLen - Len_i), 'constant', 0)
            Y_i_long_mask = functional.pad(Y_i_long_mask, (0, MaxLen - Len_i), 'constant', 0)
            Y_i_long_mask[:,:,:args.pred_start_point-1] = 0.0
            X.append(X_i)
            Y_long.append(Y_i_long)
            Y_long_mask.append(Y_i_long_mask)
            
        X = torch.stack(X, dim = 0)  #(batch_size, variables, Max-pre_win)
        Y_long = torch.stack(Y_long, dim = 0) #(batch_size, variables, pre_win, MaxLen-pre_win)
        Y_long_mask = torch.stack(Y_long_mask, dim = 0) #(batch_size, variables, pre_win, MaxLen-pre_win)
        Y_long = Y_long.permute(0,3,2,1) #(batch_size, MaxLen - pre_win , pre_win, m)
        Y_long_mask = Y_long_mask.permute(0,3,2,1) #(batch_size, MaxLen - pre_win, pre_win, m)
                
        Y_hat_list = model(X) #(batch, Max-pre_win, pre_win, m)
        Y_long_mask = Variable(Y_long_mask)
        Y_long = Variable(Y_long)
        
        if args.cuda:
            Y_long_mask = Y_long_mask.cuda()
            Y_long = Y_long.cuda()
        
        residuals = []
        for cluster_i in range(args.NumCluster):
            residuals_tmp = criterion(Y_hat_list[cluster_i]*Y_long_mask, Y_long*Y_long_mask)
            residuals.append(torch.sum(residuals_tmp, dim=(1,2,3)))
            residuals_alldim_all[cluster_i].append(residuals_tmp.cpu().detach().numpy())
        
        residuals = torch.stack(residuals, dim = 1)
        new_soft_vectors.append(residuals)
        loss_reg = torch.sum(torch.min(residuals, dim=1)[0]) / (np.sum(Y_long_mask.cpu().detach().numpy())) 
        pred_labels.append(torch.min(residuals, dim=1)[1])
        
        GraphReg = 0.0

        batch_loss = loss_reg     

        batch_loss.backward()
        total_loss += batch_loss.data.item()
        total_loss_reg += loss_reg.data.item()    
        #total_loss_left += GraphReg.item()
        total_loss_left += GraphReg

        total_time = total_time + time.time() - begin_time  
        
        optim.step()
        n_samples += batch_size
        count += 1

    total_loss /= (n_samples)
    total_loss_reg /= (n_samples)
    total_loss_cos /= count
    total_loss_left /= count
    total_loss_right /= count

    true_labels = np.concatenate(true_label_all, axis = 0)
    pred_labels = (torch.cat(pred_labels, dim = 0)).cpu().detach().numpy()

    for clust_i in range(args.NumCluster):
        residuals_alldim_all[clust_i] = np.concatenate(residuals_alldim_all[clust_i], axis = 0)

    new_soft_vectors = (torch.cat(new_soft_vectors, dim = 0)).cpu().detach().numpy()

    return total_time, total_loss, total_loss_reg, total_loss_cos, total_loss_left, total_loss_right, n_samples, pred_labels, true_labels, new_soft_vectors, residuals_alldim_all

print("begin training")
total_loss_set1 = []
total_loss_reg_set1 = []
total_loss_cos_set1 = []
total_loss_left_set1 = []
total_loss_right_set1 = []
residuals_set1 = []

for epoch in range(0, args.epochs):
    epoch_time, total_loss1, total_loss_reg1, total_loss_cos1, total_loss_left1, total_loss_right1, n_samples, pred_labels, true_labels, residuals, residuals_alldim = train(Data, Data.train, model, criterion, optim, args)
    epoch_time, total_loss2, total_loss_reg2, total_loss_cos2, total_loss_left2, total_loss_right2, n_samples1, pred_labels1, true_labels1, residuals1, residuals_alldim1 = test(Data, Data.train, model, criterion, optim, args)

    optim.updateLearningRate(epoch)
    
    total_loss_set1.append(total_loss1)
    total_loss_reg_set1.append(total_loss_reg1)
    total_loss_cos_set1.append(total_loss_cos1)
    total_loss_left_set1.append(total_loss_left1)
    total_loss_right_set1.append(total_loss_right1)

    residuals_set1.append(residuals1)
    if epoch == 0:
        print('Training Samples per cluster {:3d}'.format(n_samples1 // args.NumCluster))

    min_res_NMI1 = normalized_mutual_info_score(true_labels1, pred_labels1)
    adjusted_random_index1 = adjusted_rand_score(true_labels1, pred_labels1)
    
    if len(np.unique(pred_labels1)) >= len(np.unique(true_labels1)):
        permutation1 = find_permutation(args.NumCluster, true_labels1, pred_labels1)
        new_labels1 = [ permutation1[label] for label in pred_labels1]   # permute the labels 
        min_res_ACC1 = accuracy_score(true_labels1, new_labels1)        
    else:
        min_res_ACC1 = -1
        
    print('Epoch {:3d}|time:{:5.2f}s|total_loss_train:{:5.4f}|regression_loss_train:{:5.4f}|cos_reg_loss:{:5.4f}|left_graph_loss:{:5.4f}|right_graph_loss:{:5.4f}|'.format(epoch, epoch_time, total_loss1, total_loss_reg1, total_loss_cos1, total_loss_left1, total_loss_right1))
    print('MinRes NMI {:5.4f}, Adjusted Random Index {:5.4f}, Accuracy {:5.4f}'.format(min_res_NMI1, adjusted_random_index1, min_res_ACC1))

    CGraphlist = model.predict_relationship_inside()