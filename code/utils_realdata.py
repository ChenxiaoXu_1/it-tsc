import torch
import numpy as np;
from torch.autograd import Variable
from scipy.io import loadmat
import math
from scipy.stats import norm as ssnorm
import statistics
from scipy.stats import norm as ssnorm
import scipy.io as sio

class Data_utility(object):
    def __init__(self, args):
        self.cuda = args.cuda
        self.model = args.model
        
        self.data_name = args.data_name        
        self.data_source = args.data_source
        self.random_shuffle = args.random_shuffle
        
        self.data_path = args.data_path
        self.data_normalization = args.data_normalization
        self.data_rolling_mean = args.data_rolling_mean
                
        self.load_dataset()
        self.tensor_reform()
        
    def load_dataset(self):
        self.X_raw = np.load(self.data_path + self.data_source + '/' + self.data_name + '_X.npy', allow_pickle=True)
        self.labels = np.load(self.data_path + self.data_source + '/' + self.data_name + '_Labels.npy')     
        
        self.NumCluster = len(np.unique(self.labels))
        
        if np.min(self.labels) != 0:
            print('Labels are not 0-based index')
            if np.max(self.labels) == self.NumCluster:
                print('Labels are 1-based index')
                self.labels = self.labels - 1
            else:
                print('Error in labels!!!!!')
        
        self.TotalExample = len(self.X_raw)
        self.m = self.X_raw[0].shape[0]
            
    def _global_standard_normalization(self):
        self.SeqLen = []
        self.X = []
        
        for example_i in range(len(self.X_raw)):
            self.SeqLen.append(self.X_raw[example_i].shape[1])
        MaxLen = max(self.SeqLen)
        
        X_nan = np.zeros((self.TotalExample, self.m, MaxLen))
        X_nan[:] = np.nan
        
        for example_i in range(self.TotalExample):
            X_nan[example_i][:,:self.SeqLen[example_i]] = self.X_raw[example_i][:,:self.SeqLen[example_i]]
        
        for variable_i in range(self.m):
            mean = np.nanmean(X_nan[:,variable_i,:])
            std = np.nanstd(X_nan[:,variable_i,:])
            
            X_nan[:,variable_i,:] = (X_nan[:,variable_i,:] - mean) / std
        
        for example_i in range(self.TotalExample):
            self.X.append(X_nan[example_i,:,:self.SeqLen[example_i]])

    def _no_normalization(self):
        self.SeqLen = []
        self.X = []
                    
        for example_i in range(self.TotalExample):
            self.SeqLen.append(self.X_raw[example_i].shape[1])
            data_norm = np.zeros((self.X_raw[example_i].shape))           
            for variable_i in range(self.m):
                data_norm[variable_i,:] = self.X_raw[example_i][variable_i,:]
            self.X.append(data_norm) 
                                
    def _global_copula(self, X):
        Data = X.transpose((1,0,2)).reshape((self.m, self.TotalExample*self.MaxLen)) #(variables, examples, timestamps)
        T = Data.shape[1]
        Data_norm = np.zeros(Data.shape)
            
        delta = 1/(4*math.pow(T,1/4)*math.sqrt(math.pi*math.log(T)))
        
        for i in range(T):
            Data_norm[:,i] = np.sum(Data < Data[:,i:i+1], axis=1) / T

        Data_norm[Data_norm < delta] = delta
        Data_norm[Data_norm > 1-delta] = 1-delta
           
        Data_norm = ssnorm.ppf(Data_norm)        
        X_copula = Data_norm.reshape((self.m, self.TotalExample, self.MaxLen)).transpose((1,0,2))
        return X_copula
    
    def tensor_reform(self):
        if self.data_normalization == 'global_standard':
            self._global_standard_normalization()
        elif self.data_normalization == 'global_copula':
            self._no_normalization()            
        
        self.SeqLen = np.array(self.SeqLen) #(Example, )
        self.MaxLen = max(self.SeqLen)
    
        X_train = np.zeros((self.TotalExample, self.m, self.MaxLen)) #(examples, m, seq_len)
        for example_i in range(self.TotalExample):
            seqLen = self.X[example_i].shape[1]
            X_train[example_i,:,:] = np.pad(self.X[example_i], ((0,0), (0,self.MaxLen-seqLen)), 'constant')
            
        if self.data_normalization == 'global_copula':
            X_train = self._global_copula(X_train)
            
        X_valid = X_train.copy()
                
        label_train = self.labels
        label_valid = self.labels.copy()
        
        SeqLen_train = self.SeqLen.copy()
        SeqLen_valid = self.SeqLen.copy()
                
        if self.random_shuffle:
            random_index = np.random.permutation(X_train.shape[0])            
            X_train = X_train[random_index]            
            label_train = label_train[random_index]
            SeqLen_train = SeqLen_train[random_index]
        
        X_train = torch.tensor(X_train, dtype=torch.float)
        X_valid = torch.tensor(X_valid, dtype=torch.float)               

        self.train = [X_train, label_train, SeqLen_train]
        self.valid = [X_valid, label_valid, SeqLen_valid]
            
    def get_batches(self, data, batch_size, shuffle = False):
        
        inputs = data[0]
        labels = data[1]
        seqLens = data[2]

        length = len(inputs)
        if shuffle:
            index = torch.randperm(length)
        else:
            index = torch.LongTensor(range(length))
        start_idx = 0
        
        while (start_idx < length):
            end_idx = min(length, start_idx + batch_size)
            excerpt = index[start_idx:end_idx]
            sequence = inputs[excerpt]
            label = labels[excerpt]
            seqLen = seqLens[excerpt]
            
            if sequence.shape[0] == 1:
                label = np.array([label])
                seqLen = np.array([seqLen])
            
            sequence = Variable(sequence)
            if (self.cuda):
                sequence = sequence.cuda()

            data = [sequence, label, seqLen]
            yield data
            start_idx += batch_size