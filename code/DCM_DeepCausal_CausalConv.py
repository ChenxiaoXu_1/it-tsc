import torch
import torch.nn as nn
import torch.nn.functional as F
#from torch.nn.parameter import Parameter
import DilatedCausalConv_layer, FinalRowWise_layer, GraphLearning_layer
import numpy as np

from torch.nn.utils import weight_norm

## granger full rank
class Model(nn.Module):
    def __init__(self, args, data):
        super(Model, self).__init__()
        self.use_cuda = args.cuda
        self.m = data.m

        self.batch_size = args.batch_size
        self.NumCluster = args.NumCluster
        self.MaxLen = args.MaxLen

        self.double_causal_perLayer = args.double_causal_perLayer
        self.Q = args.NumCausalLayer1
        self.kernel_size1 = args.kernel_size1
        self.channel_multiplier = args.channel_multiplier
        self.channel_list = args.channel_list.copy()
        self.channel_list.insert(0, 1)

        self.low_rank = args.low_rank   
        self.Graph_activate = args.Graph_activate             
        self.reduce_dim = args.reduce_dim.copy()

        self.pre_win = args.pre_win
        self.tanh_alpha = args.tanh_alpha

        self.totalQ = 0
        ### Module 1: define CausalLayer
        self.CausalLayerList = []
        for layer_i in range(self.Q):
            dilation_size = 2 ** layer_i
            in_channel = self.channel_multiplier ** layer_i
            out_channel = self.channel_multiplier ** (layer_i+1)            
            
            self.totalQ += out_channel
            self.CausalLayerList.append(DilatedCausalConv_layer.DilatedCausalConv(args, dilation_size, in_channel, out_channel))
        self.CausalLayerList = nn.ModuleList(self.CausalLayerList)
        self.clust_net = []
        self.reduce_dim.insert(0, self.totalQ)
        
        for cluster_i in range(self.NumCluster):
            single_clust_net = []
            #Graph embedding
            clust_graph_net = GraphLearning_layer.LowRankGraph(args, data, self.totalQ)            
          #  clust_graph_net = GraphLearning_layer.IdentityGraph(args, data, self.totalQ)            
            #compress on intervariable nonlinearity
            compress_net = []
            for layer_i in range(len(self.reduce_dim)-1):
                compress_net.append(weight_norm(nn.Linear(self.reduce_dim[layer_i], self.reduce_dim[layer_i+1]) ))             

            single_clust_net.append(clust_graph_net) 
            single_clust_net.append(nn.ModuleList(compress_net))
            #Row-wise product
            single_clust_net.append(FinalRowWise_layer.FR_Model(args, data)) 
            single_clust_net = nn.ModuleList(single_clust_net)
            self.clust_net.append(single_clust_net)
        
        self.clust_net = nn.ModuleList(self.clust_net)
        self.dropout = nn.Dropout(args.dropout)
                      
    def forward(self, inputs):
        x0 = inputs #(batch_size, m, timestamps) 
        batch_size, pre_win_long = x0.shape[0], x0.shape[-1]
        ### Module 1
        
        z_p = []
        for layer_i in range(self.Q):
            if layer_i == 0:
                x0 = x0.unsqueeze(2) #(batch, m, channel, timestamps)  channel = 1
            x1 = self.CausalLayerList[layer_i](x0)
            # z = x1[:,:,:,:] #(batch, m, channel, timestamps)            
            # z_list.append(z)
            z_p.append(x1)
            x0 = x1
        z_p = torch.cat(z_p, dim = 2) #(batchsize, m, pQ, pre_win_long)
                    
        x0 = inputs
        x1 = inputs
        
        z_p = z_p.permute((0,3,1,2)) #(batchsize, pre_win_long, m, pQ)
        predict_timestamp = z_p.shape[1]
        z_p = z_p.reshape((batch_size*predict_timestamp, self.m, self.totalQ)) #(batch*pre_win_long, m, pQ)
        final_y_tensor_list = []
        
        for cluster_i in range(self.NumCluster):
            #Left graph learning
            x_sp = z_p.transpose(2,1) #(batch*pre_win_long, pQ, m)
            x_sp = self.clust_net[cluster_i][0](x_sp, batch_size) #(batch*pre_win_long, pQ, m)
            if self.Graph_activate == 'elu':
                x_sp = F.elu(x_sp)
            elif self.Graph_activate == 'tanh':
                x_sp = F.tanh(x_sp)
            
            #intervariable nonlinearity
            x_sp = x_sp.transpose(2,1) #(batch*pre_win_long, m, pQ)
            for layer_i in range(len(self.reduce_dim)-1):
                x_sp = self.clust_net[cluster_i][1][layer_i](x_sp) #(batch*pre_win_long, m, reduce_dim)
                x_sp = F.tanh(x_sp/self.tanh_alpha)
                x_sp = self.dropout(x_sp)
    
            #rowwise product
            x_sp = self.clust_net[cluster_i][2](x_sp) #(batch*pre_win_long, pre_win, m)
            final_y = x_sp.reshape((batch_size, predict_timestamp, self.pre_win, self.m)) #(batchsize, pre_win_long, pre_win, m)
            final_y_tensor_list.append(final_y)
           
        return final_y_tensor_list  

    def predict_relationship_inside(self):
        CGraph_list = []
        for cluster_i in range(self.NumCluster):
            LeftGraph = self.clust_net[cluster_i][0].LeftGraph.weight.data
            RightGraph = self.clust_net[cluster_i][0].RightGraph.weight.data
            CGraph = torch.matmul(RightGraph, LeftGraph).transpose(1,0).detach().cpu().numpy()
            #elfRegulator = self.clust_net[cluster_i][0].Identity.data.detach().cpu().numpy()
            #raph_list.append([CGraph, SelfRegulator])
            CGraph_list.append([CGraph])
                   
        return CGraph_list
         
