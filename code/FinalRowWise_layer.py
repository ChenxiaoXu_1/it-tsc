import torch
import torch.nn as nn
import pdb
import torch.nn.functional as F
from torch.nn.parameter import Parameter
import numpy as np
from sklearn.preprocessing import normalize
import math
## granger full rank

class FR_Model(nn.Module):
    def __init__(self, args, data):
        super(FR_Model, self).__init__()
        self.pre_win = args.pre_win
        self.m = data.m
        self.reduce_dim = args.reduce_dim[-1]
        self.cuda = args.cuda

        self.weight = nn.Parameter(torch.ones([self.m, self.reduce_dim, self.pre_win]))   
        self.bias = Parameter(torch.Tensor(self.m, self.pre_win))                             
        nn.init.kaiming_uniform_(self.weight, mode='fan_in', nonlinearity='relu')
        fan_in, _ = nn.init._calculate_fan_in_and_fan_out(self.weight)
        bound = 1 / math.sqrt(fan_in)
        nn.init.uniform_(self.bias, -bound, bound)

        # self.weight = nn.Parameter(torch.ones([self.m, self.reduce_dim, self.pre_win]))   
        # self.bias = Parameter(torch.Tensor(self.m, self.pre_win))                
        # nn.init.uniform_(self.weight, a = -0.5, b = 0.5)
        # nn.init.uniform_(self.bias, a = -0.5, b = 0.5)

    def forward(self, x):
        if self.pre_win ==1:
            final_y = torch.empty(x.shape[0], self.m) 
        else :
            final_y = torch.empty(x.shape[0], self.pre_win, self.m)  #(batchsize, pre_win, variables)
        
        for j in range(self.m):           
            if self.pre_win ==1:   
                final_y[:,j] = F.linear(x[:,j,:], self.weight[j,:].view(1, self.weight.shape[1]), self.bias[j,:]).view(-1)              
            else:
                final_y[:,:,j] = F.linear(x[:,j,:], self.weight[j,:].transpose(1,0), self.bias[j,:]);               
        
        if self.pre_win == 1:
            final_y.unsqueeze_(1)
        
        if self.cuda:
            final_y = final_y.cuda()
        return final_y
